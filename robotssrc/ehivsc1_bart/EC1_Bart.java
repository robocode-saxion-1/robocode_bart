package ehivsc1_bart;

import robocode.Robot;
import robocode.ScannedRobotEvent;

/**
 * Created by bartv on 10-2-2016.
 */
public class EC1_Bart extends Robot{

    public void run() {

           while(true){
              turnGunLeft (10);
           }
    }


    public void setStartPosition(){
        double degreesToStart = 360 - getHeading ();
        if(degreesToStart < 180){
            turnRight (360 - getHeading ());
        }
        else{
            turnLeft (0 + getHeading ());
        }
    }

    //looks if a the robot isnt in a 50 range of a wall
    private boolean noWall(){

      if(getY () < getBattleFieldHeight () - 50 && getY () > 50 && getX () < getBattleFieldWidth () - 50  && getX () > 50){
          return true;
      }
        return false;
    }
    private void turnAway(){
        if(getY () < 60){
            setStartPosition ();
            ahead (50);
        }
        else if(getY () > ( getBattleFieldHeight () - 60)){
            setStartPosition ();
            turnLeft (180);
            ahead (50);
        }
        else if(getX () < 60){
            setStartPosition ();
            turnLeft (45);
        }
        else if(getX () > ( getBattleFieldWidth () - 60)){
            setStartPosition ();
            turnRight (180);
            ahead (50);
        }
    }
    @Override
    public void onScannedRobot(ScannedRobotEvent event)
    {
        super.onScannedRobot (event);
        fire (3);
        turnLeft (event.getBearing ());
        if(noWall ()){
            ahead (50);
            System.out.println ("Geen muur");
        }
        else
        {
            turnAway ();
            System.out.println ("Muur");
        }
    }
}
