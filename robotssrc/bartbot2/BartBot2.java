package bartbot2;

import robocode.AdvancedRobot;
import robocode.HitRobotEvent;
import robocode.ScannedRobotEvent;

import java.awt.*;

import static robocode.util.Utils.normalRelativeAngleDegrees;


/**
 * Created by Bart on 11-2-2016.
 */
public class BartBot2 extends AdvancedRobot{
    public void run() {
        while(true){
            turnRadarRight(360);
        }
    }

    @Override
    public void onScannedRobot(ScannedRobotEvent e) {
        super.onScannedRobot(e);
        System.out.println(e.getBearing());
        double gunTurnAmt;
        gunTurnAmt = normalRelativeAngleDegrees(e.getBearing() + (getHeading() - getRadarHeading()));

        turnGunRight(gunTurnAmt); // Try changing these to setTurnGunRight,
        turnRight(e.getBearing());
        ahead(10);
    }

}
