package ehivsc1_bartv2;

import robocode.*;
import java.awt.*;
import java.util.ArrayList;

import robocode.AdvancedRobot;

/**
 * Created by bartv on 17-2-2016.
 */
public class EC1_Bartv2 extends AdvancedRobot
{
    boolean nearWall; // Is true when robot is near the wall.
    int startingRobots;

    ArrayList<ScannedRobotEvent> enemys = new ArrayList<> ();
    ArrayList<EnemyInfo> enemysInfo = new ArrayList<> ();
    public void run() {

        startingRobots = getOthers ();

        //Set the color to black
        setAllColors (new Color (255,255,255));

        //Moves the parts freely from each other
        setAdjustGunForRobotTurn (true);
        setAdjustRadarForGunTurn (true);
        setAdjustRadarForRobotTurn (true);
        while(true){
            if (getX() > 50 && getY() > 50 && getBattleFieldWidth() - getX() > 50 && getBattleFieldHeight() - getY() > 50 && nearWall == true) {
                nearWall = false;
            }
            if (getX() <= 50 || getY() <= 50 || getBattleFieldWidth() - getX() <= 50 || getBattleFieldHeight() - getY() <= 50 ) {
                if ( nearWall == false){
                    turnLeft (180);
                    nearWall = true;
                }
            }
            turnRadarLeft (360);
        }
    }

    @Override
    public void onScannedRobot(ScannedRobotEvent event) {
        boolean uniqueEnemy = true;

        if(enemys.size () < startingRobots) {
            //excuted when not all robots are aready in the arraylist
            for (ScannedRobotEvent Robot : enemys) {
                if (event.getName () == Robot.getName ()) {
                    uniqueEnemy = false;
                    break;
                }
            }
            if (uniqueEnemy) {
                enemys.add (event);
                EnemyInfo enemyInfo = new EnemyInfo ();
                enemyInfo.setEnemyBot (event);
                enemyInfo.setPrevEnery (event.getEnergy ());
                enemysInfo.add (enemyInfo);
            }
        }
        //excuted when scanned
        for(ScannedRobotEvent Robot:enemys){
            if(Robot.getName () == event.getName ()){
                for(int i = 0;i < enemysInfo.size ();i++){
                    EnemyInfo enemy = enemysInfo.get (i);
                    if(enemy.getEnemyBot ().getName () == Robot.getName ()){
                        System.out.println (enemy.getPrevEnery ());
                        if(enemy.getPrevEnery () > event.getEnergy ()){
                            System.out.println (event.getBearing ());
                            System.out.println ("Er is geschoten");
                            ahead (50);
                            enemy.setPrevEnery (event.getEnergy ());
                        }
                        enemy.setPrevEnery (event.getEnergy ());
                    }
                }
            }
        }

    }
}
