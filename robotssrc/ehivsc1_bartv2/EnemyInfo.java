package ehivsc1_bartv2;

import robocode.ScannedRobotEvent;

/**
 * Created by bartv on 17-2-2016.
 */
public class EnemyInfo
{
    private ScannedRobotEvent enemyBot;
    private double prevEnery;

    public ScannedRobotEvent getEnemyBot()
    {
        return enemyBot;
    }

    public void setEnemyBot(ScannedRobotEvent enemyBot)
    {
        this.enemyBot = enemyBot;
    }

    public double getPrevEnery()
    {
        return prevEnery;
    }

    public void setPrevEnery(double prevEnery)
    {
        this.prevEnery = prevEnery;
    }
}
